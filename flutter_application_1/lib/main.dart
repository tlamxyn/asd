import 'package:flutter/material.dart';
import 'dart:math';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final String title;

  const MyHomePage({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String ketqua = 'Chưa nhập số nguyên';

  TextEditingController songuyenController = TextEditingController();

  //Kiểm tra số nguyên tố(SNT): trả về true nếu là SNT
  bool checkSNT(int n) {
    if(n < 2) {
      return false;
    }
    int a = sqrt(n).floor();
    int count = 0;
    for (int i = 2; i <= a; i++) {
      if (n % i == 0) {
        count++;
      }
    }
    if (count != 0) {
      return false;
    }
    return true;
  }

  void printResult(String t) {
    if (t == "") {
      ketqua = 'Chưa nhập số nguyên';
    } else {
      int snt = int.parse(t);

      if (checkSNT(snt)) {
        ketqua = 'Số $snt vừa nhập là số nguyên tố ';
      } else {
        ketqua = 'Số $snt vừa nhập không là số nguyên tố ';
      }
    }
  }

  @override
  void dispose() {
    songuyenController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        padding: const EdgeInsets.all(15),
        child: ListView(
          children: [
            TextField(
              keyboardType: TextInputType.number,
              controller: songuyenController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Nhập số nguyên',
              ),
            ),
            const SizedBox(height: 10),
            ElevatedButton(
              child: const Text('Kiểm tra'),
              onPressed: () {
                setState(() {
                  printResult(songuyenController.text);
                });
              },
            ),
            const SizedBox(height: 10),
            Text(
              ketqua,
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
